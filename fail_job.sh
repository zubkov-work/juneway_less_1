#!/bin/bash

jobs=`curl -s -X GET -H "private-token: $PRIVATE_TOKEN" https://gitlab.com/api/v4/projects/25490769/jobs?scope=failed`

echo $jobs | jq '.[] | select(.name == "build prod") | .id' | head -n 1
