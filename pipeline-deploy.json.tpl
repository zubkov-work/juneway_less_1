{
  "ref": "master", 
  "variables": [
    { "key": "IMAGE", "variable_type": "env_var", "value": "$CI_REGISTRY_IMAGE" },
    { "key": "TAG", "variable_type": "env_var", "value": "$CI_COMMIT_SHORT_SHA" }
  ]
}